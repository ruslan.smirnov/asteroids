using System;
using UnityEngine;
using UnityEngine.UI;

public sealed class GameOverWindow : MonoBehaviour {
	const string ScoreFormat = "Score: {0}";

	[SerializeField]
	GameObject _window;
	[SerializeField]
	Text   _scoreText;
	[SerializeField]
	Button _restartButton;

	public Action RestartButtonClick;

	void Awake() {
		_restartButton.onClick.AddListener(() => RestartButtonClick?.Invoke());
		_window.SetActive(false);
	}

	public void Show(int score) {
		_scoreText.text = string.Format(ScoreFormat, score);
		_window.SetActive(true);
	}

	public void Hide() {
		_window.SetActive(false);
	}
}