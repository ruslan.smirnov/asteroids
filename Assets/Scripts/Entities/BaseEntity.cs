using System;
using System.Collections.Generic;

public abstract class BaseEntity {
	readonly Dictionary<Type, BaseComponent> _component = new Dictionary<Type, BaseComponent>();

	// у Entity должен быть id для быстрого поиска

	public void AddComponent<T>(T component) where T : BaseComponent {
		_component.Add(typeof(T), component);
	}

	public T GetComponent<T>() where T : BaseComponent {
		_component.TryGetValue(typeof(T), out var value);
		return value as T;
	}
}