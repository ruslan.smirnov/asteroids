public sealed class DestroyableComponent : BaseComponent {
	public DestroyableConfig Config { get; private set; }
	public int HealthPoints { get; private set; }

	public bool IsAvableDestroy => HealthPoints <= 0;

	public void Setup(DestroyableConfig config) {
		Config = config;
		HealthPoints = Config.HealthPoints;
	}

	public bool TryDecHealth(string tag) {
		// добавить учет от tag - типа урона и его количества (для луча)
		if ( Config.Enemies.Contains(tag) ) {
			HealthPoints = tag == "Ray" ? 0 : HealthPoints - 1;
			return true;
		}
		return false;
	}
}