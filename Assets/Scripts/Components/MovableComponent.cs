using UnityEngine;

public class MovableComponent : BaseComponent {
	Transform _transform;
	Vector2   _forceDirection;

	public MovableConfig Config { get; private set; }

	public float Speed { get; private set; }
	public float Angle { get; private set; }
	public Vector2 Position => _transform != null ? (Vector2) _transform.position : Vector2.zero;

	public void Setup(MovableConfig config, Transform transform, float angleDirection) {
		Config = config;
		_transform = transform;
		Angle = angleDirection;
		Speed = Config.FixedMoving ? Config.MaxSpeed : 0;
	}

	public virtual void Update() {
		MoveUpdate(GameHandler.AxisHorizontal, GameHandler.AxisVertical);
	}

	protected void MoveUpdate(float axisHorizontal, float axisVertical) {
		// фиксировать для FixedMoving параметры CurrentAngle, _forceDirection, CurrentSpeed
		var movable = axisVertical > 0;
		if ( (axisHorizontal != 0) || (axisVertical > 0) ) {
			var angle = Config.RotateSpeed * axisHorizontal * -1;
			Angle = (Angle + angle + 360) % 360;
		}

		if ( movable && !Config.FixedMoving ) {
			if ( Speed < Config.MaxSpeed ) {
				Speed += Config.MinSpeed * axisVertical;
			}
		}

		if ( !Config.FixedMoving ) {
			Speed = Speed > 0 ? (Speed - Time.deltaTime / Config.ForceDecrease) : 0;
		}

		if ( Speed > 0 ) {
			if ( movable ) {
				var angle = Angle * Mathf.Deg2Rad;
				var x = Mathf.Cos(angle);
				var y = Mathf.Sin(angle);
				_forceDirection = new Vector3(x, y);
			}
			_transform.Translate(_forceDirection * (Time.deltaTime * Speed));
		}
	}
}