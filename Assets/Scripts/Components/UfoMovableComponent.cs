using UnityEngine;

public sealed class UfoMovableComponent : MovableComponent {
	const float AcceptableAngleError = 2.0f;

	public override void Update() {
		var angle = Angle;
		Vector2 target = GameHandler.Instance.EntityHandler.ShipContainer.View.transform.position;
		Vector2 delta = target - Position;

		var targetAngle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;
		var angleError = Mathf.DeltaAngle(targetAngle, angle);
		var nextAngle = angle;
		if (Mathf.Abs(angleError) > AcceptableAngleError) {
			var config = Config as UfoConfig;
			nextAngle = Mathf.MoveTowardsAngle(angle, targetAngle, config.TargetRotateSpeed);
			nextAngle = (nextAngle + 360) % 360;
		}

		var diff = angle - nextAngle;
		MoveUpdate(diff, 1);
	}
}