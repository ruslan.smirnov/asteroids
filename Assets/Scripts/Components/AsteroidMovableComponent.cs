using UnityEngine;

public sealed class AsteroidMovableComponent : MovableComponent {
	public override void Update() {
		var angle = Random.Range(-1, 1);
		MoveUpdate(angle, 1);
	}
}