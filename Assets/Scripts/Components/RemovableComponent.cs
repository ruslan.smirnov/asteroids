using UnityEngine;

public sealed class RemovableComponent : BaseComponent {
	public Transform Transform { get; private set; }

	public void Setup(Transform transform) {
		Transform = transform;
	}
}