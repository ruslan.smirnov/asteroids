using UnityEngine;

public sealed class ProjectileMovableComponent : MovableComponent {
	public override void Update() {
		MoveUpdate(0, 1);
	}
}