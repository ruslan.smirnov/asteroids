using UnityEngine;

public sealed class TeleportableComponent : BaseComponent {
	public Transform Transform { get; private set; }

	public void Setup(Transform transform) {
		Transform = transform;
	}
}