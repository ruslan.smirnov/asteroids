using UnityEngine;

public sealed class GameHandler : MonoBehaviour {
	public static GameHandler Instance { get; private set; }

	[SerializeField]
	GameConfig _gameConfig;

	[SerializeField]
	GameUI _ui;

	[SerializeField]
	UnityRepository _unityRepository;

	[SerializeField]
	GameOverWindow _gameOverWindow;

	PlayableBehaviour _playableBehaviour;
	int               _score;
	float             _spawnTimer;

	public GameField GameField { get; private set; }
	public EntityHandler EntityHandler { get; private set; }
	public UnityRepository UnityRepository => _unityRepository;

	public static float AxisHorizontal => Input.GetAxis("Horizontal");
	public static float AxisVertical => Input.GetAxis("Vertical");

	void Awake() {
		Instance = this;

		GameField = new GameField();
		GameField.Update();

		EntityHandler = new EntityHandler(UnityRepository);

		EntityHandler.EnemyDestroy  += OnEnemyDestroyed;
		EntityHandler.PlayerDestroy += OnPlayerDestroyed;

		_gameOverWindow.RestartButtonClick += OnRestartButtonClicked;

		_playableBehaviour = EntityHandler.PlayableBehaviour;

		_playableBehaviour.UpdateAngle       += _ui.UpdateAngle;
		_playableBehaviour.UpdatePosition    += _ui.UpdatePosition;
		_playableBehaviour.UpdateChargers    += _ui.UpdateRayCharges;
		_playableBehaviour.UpdateReloadTimer += _ui.UpdateRayTimer;
		_playableBehaviour.UpdateSpeed       += _ui.UpdateSpeed;
		_playableBehaviour.UpdateHealth      += _ui.UpdateHealth;

		InitUI();
	}

	void Update() {
		GameField.Update();
		EntityHandler.Update();
		_playableBehaviour.Update();

		if ( Input.GetKeyDown(KeyCode.Space) ) {
			if ( _playableBehaviour.TryShootProjectile() ) {
				EntityHandler.CreateProjectile();
			}
		}

		if ( Input.GetKeyDown(KeyCode.X) ) {
			_playableBehaviour.TryShootRay();
		}

		if ( _spawnTimer > 0 ) {
			_spawnTimer -= Time.deltaTime;
		} else {
			_spawnTimer = _gameConfig.SpawnTime;
			if ( EntityHandler.AsteroidCount < _gameConfig.AsteroidCount ) {
				EntityHandler.CreateAsteroid(AsteroidSize.Large, GameField.GetRandomAngle());
			}
			if ( EntityHandler.UfoCount < _gameConfig.UfoCount ) {
				EntityHandler.CreateUfo(GameField.GetRandomAngle());
			}
		}
	}

	public void HandleCollision(BaseView view, Collider2D other) {
		EntityHandler.HandleCollision(view, other);
	}

	void InitUI() {
		var shipContainer = EntityHandler.ShipContainer;
		var healthPoints = shipContainer.Entity.GetComponent<DestroyableComponent>().HealthPoints;
		var position = shipContainer.Entity.GetComponent<MovableComponent>().Position;
		var angle = shipContainer.Entity.GetComponent<MovableComponent>().Angle;

		_ui.UpdateHealth(healthPoints);
		_ui.UpdateRayCharges(_playableBehaviour.Chargers, _unityRepository.ShipConfig.RayCharges);
		_ui.UpdateRayTimer(_playableBehaviour.ReloadTimer);
		_ui.UpdateAngle(angle);
		_ui.UpdatePosition(position);
		_ui.UpdateScore(0);
		_ui.UpdateSpeed(0);
	}

	void OnEnemyDestroyed(int value) {
		_score += value;
		_ui.UpdateScore(_score);
	}

	void OnPlayerDestroyed() {
		_gameOverWindow.Show(_score);
	}

	void OnRestartButtonClicked() {
		_score = 0;
		_gameOverWindow.Hide();

		EntityHandler.CreateShip();
		InitUI();
	}
}