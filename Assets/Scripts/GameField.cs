using System;
using UnityEngine;
using Random = UnityEngine.Random;

public sealed class GameField {
	public const float TeleportAngularLimit = 0.3f;
	public const float TeleportlinearLimit  = 0.05f;

	public float Height       { get; private set; }
	public float Width        { get; private set; }

	public float HeightHalf   { get; private set; }
	public float WidthHalf    { get; private set; }

	public float Diagonal     { get; private set; }

	public void Update() {
		Height     = Camera.main.orthographicSize * 2.0f;
		Width      = Height * Screen.width / Screen.height;
		HeightHalf = Height / 2;
		WidthHalf  = Width / 2;
		Diagonal   = (float)Math.Sqrt(Math.Pow(Height, 2) + Math.Pow(Width, 2));
	}

	public Vector3 GetRandomAngle() {
		var xRange = Random.Range(0, 2);
		var yRange = Random.Range(0, 2);

		var x = xRange == 1 ? WidthHalf : -1 * WidthHalf;
		var y = yRange == 1 ? HeightHalf : -1 * HeightHalf;

		return new Vector3(x, y);
	}
}