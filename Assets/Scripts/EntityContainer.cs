public readonly struct EntityContainer {
	public readonly BaseEntity Entity;
	public readonly BaseView   View;

	public EntityContainer(BaseEntity entity, BaseView view) {
		Entity = entity;
		View = view;
	}
}