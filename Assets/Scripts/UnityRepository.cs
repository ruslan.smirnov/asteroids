using System.Collections.Generic;
using UnityEngine;

public sealed class UnityRepository : MonoBehaviour {
	[field: SerializeField] public ShipConfig ShipConfig { get; private set; }
	[field: SerializeField] public DestroyableConfig ShipDestroyableConfig { get; private set; }
	[field: SerializeField] public ShipView ShipView { get; private set; }

	[field: SerializeField] public ProjectileConfig ProjectileConfig { get; private set; }
	[field: SerializeField] public DestroyableConfig ProjectileDestroyableConfig { get; private set; }
	[field: SerializeField] public ProjectileView ProjectileView { get; private set; }

	[field: SerializeField] public RayView RayView { get; private set; }

	[field: SerializeField] public AsteroidConfig AsteroidLargeConfig { get; private set; }
	[field: SerializeField] public AsteroidConfig AsteroidMediumConfig { get; private set; }
	[field: SerializeField] public AsteroidConfig AsteroidSmallConfig { get; private set; }
	[field: SerializeField] public DestroyableConfig AsteroidLargeDestroyableConfig { get; private set; }
	[field: SerializeField] public DestroyableConfig AsteroidMediumDestroyableConfig { get; private set; }
	[field: SerializeField] public DestroyableConfig AsteroidSmallDestroyableConfig { get; private set; }
	[field: SerializeField] public AsteroidView AsteroidLargeView { get; private set; }
	[field: SerializeField] public List<AsteroidView> AsteroidMediumView { get; private set; }
	[field: SerializeField] public List<AsteroidView> AsteroidSmallView { get; private set; }

	[field: SerializeField] public UfoConfig UfoConfig { get; private set; }
	[field: SerializeField] public DestroyableConfig UfoDestroyableConfig { get; private set; }
	[field: SerializeField] public UfoView UfoView { get; private set; }

	public T CreateView<T>(T view) where T : BaseView {
		return Instantiate(view);
	}

	public void HideView(BaseView view) {
		view.Hide();
		view.SetupEntity(null);
		Destroy(view.gameObject);
	}
}