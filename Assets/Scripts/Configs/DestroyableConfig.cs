using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DestroyableConfig", menuName = "ScriptableObjects/DestroyableConfig")]
public sealed class DestroyableConfig : ScriptableObject {
	[SerializeField]
	int _healthPoints;

	[SerializeField]
	List<string> _enemies;

	[field: SerializeField]
	public int ScoreBounty { get; private set; }

	public int HealthPoints => _healthPoints;
	public List<string> Enemies => _enemies;
}