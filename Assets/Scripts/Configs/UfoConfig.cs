using UnityEngine;

[CreateAssetMenu(fileName = "UfoConfig", menuName = "ScriptableObjects/UfoConfig")]
public sealed class UfoConfig : MovableConfig {
	[SerializeField]
	float _targetRotateSpeed;

	public float TargetRotateSpeed => _targetRotateSpeed;
}