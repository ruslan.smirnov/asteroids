using UnityEngine;

[CreateAssetMenu(fileName = "ProjectileConfig", menuName = "ScriptableObjects/ProjectileConfig")]
public sealed class ProjectileConfig : MovableConfig {
}