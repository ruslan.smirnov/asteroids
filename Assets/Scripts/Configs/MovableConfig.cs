using UnityEngine;

public class MovableConfig : ScriptableObject {
	[SerializeField]
	bool _fixedMoving;
	[SerializeField]
	float _minSpeed;
	[SerializeField]
	float _maxSpeed;
	[SerializeField]
	float _rotateSpeed;
	[SerializeField]
	float _forceDecrease;
	[SerializeField]
	float _initAngle;

	public bool FixedMoving => _fixedMoving;
	public float MinSpeed => _minSpeed;
	public float MaxSpeed => _maxSpeed;
	public float RotateSpeed => _rotateSpeed;
	public float ForceDecrease => _forceDecrease;
	public float InitAngle => _initAngle;
}