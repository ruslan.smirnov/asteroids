using UnityEngine;

[CreateAssetMenu(fileName = "ShipConfig", menuName = "ScriptableObjects/ShipConfig")]
public sealed class ShipConfig : MovableConfig {
	[field: SerializeField]
	public float ProjectileReloadTime { get; private set; }
	[field: SerializeField]
	public float ProjectileVisualFireTime { get; private set; }

	[field: SerializeField]
	public int RayCharges { get; private set; }
	[field: SerializeField]
	public float RayReloadTime { get; private set; }
	[field: SerializeField]
	public float RayVisualFireTime { get; private set; }

	[field: SerializeField]
	public float HitVisualTime { get; private set; }
}