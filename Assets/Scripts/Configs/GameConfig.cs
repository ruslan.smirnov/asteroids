using UnityEngine;

[CreateAssetMenu(fileName = "GameConfig", menuName = "ScriptableObjects/GameConfig")]
public sealed class GameConfig : ScriptableObject {
	[field: SerializeField]
	public int AsteroidCount { get; private set; }

	[field: SerializeField]
	public int UfoCount { get; private set; }

	[field: SerializeField]
	public float SpawnTime { get; private set; }
}