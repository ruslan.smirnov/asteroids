using UnityEngine;

[CreateAssetMenu(fileName = "AsteroidConfig", menuName = "ScriptableObjects/AsteroidConfig")]
public sealed class AsteroidConfig : MovableConfig {
}