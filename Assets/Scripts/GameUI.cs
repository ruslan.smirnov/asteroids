using UnityEngine;
using UnityEngine.UI;

public sealed class GameUI : MonoBehaviour {
	const string PositionFormat = "x {0:0.00} y {1:0.00}";
	const string AngleFormat    = "deg {0:0.00}";
	const string ChargesFormat  = "charges {0} / {1}";
	const string TimerFormat    = "{0:0.00}";
	const string SpeedFormat    = "speed {0:0.00}";

	const string HealthFormat = "Health: {0}";
	const string ScoreFormat  = "Score: {0}";

	[SerializeField]
	Text _positionText;
	[SerializeField]
	Text _angleText;
	[SerializeField]
	Text _rayChargesText;
	[SerializeField]
	Text _rayTimerText;
	[SerializeField]
	Text _speedText;

	[SerializeField]
	Text _healthText;
	[SerializeField]
	Text _scoreText;

	public void UpdatePosition(Vector2 position) {
		_positionText.text = string.Format(PositionFormat, position.x, position.y);
	}

	public void UpdateAngle(float value) {
		_angleText.text = string.Format(AngleFormat, value);
	}

	public void UpdateRayCharges(int value, int max) {
		_rayChargesText.text = string.Format(ChargesFormat, value, max);
	}

	public void UpdateRayTimer(float value) {
		_rayTimerText.text = string.Format(TimerFormat, value);
	}

	public void UpdateSpeed(float value) {
		_speedText.text = string.Format(SpeedFormat, value);
	}

	public void UpdateHealth(int value) {
		_healthText.text = string.Format(HealthFormat, value);
	}

	public void UpdateScore(int value) {
		_scoreText.text = string.Format(ScoreFormat, value);
	}
}