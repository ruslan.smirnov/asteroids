using UnityEngine;

public sealed class ProjectileView : BaseView {
	void OnTriggerEnter2D(Collider2D other) {
		GameHandler.Instance.HandleCollision(this, other);
	}
}