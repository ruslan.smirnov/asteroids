using System.Collections;
using UnityEngine;

public sealed class ShipView : BaseView {
	[SerializeField]
	SpriteRenderer _ship;
	[SerializeField]
	GameObject _jet;
	[SerializeField]
	GameObject _projectile;
	[SerializeField]
	GameObject _projectileFire;
	[SerializeField]
	RayView _ray;

	ShipConfig _config;

	float _projectileVisualFireTimer;
	float _rayVisualFireTimer;

	float _hitTimer;

	void Awake() {
		InitModules();
	}

	void OnTriggerEnter2D(Collider2D other) {
		GameHandler.Instance.HandleCollision(this, other);
	}

	public void Setup() {
		InitModules();
		_config = GameHandler.Instance.UnityRepository.ShipConfig;

		// VerticalSize   6.315509 | 78.83125 | 12.49
		// HorizontalSize 3.729811 | 46.60571 | 12.52

		// VerticalSize   6.315509 | 78.83125 | 12.49
		// HorizontalSize 5.438355 | 68.00488 | 12.52

		var gf = GameHandler.Instance.GameField;
		_ray.Setup(gf.Diagonal * 12.5f, (gf.Diagonal * 12.5f) / 25);
	}

	public override void Hide() {
		StopCoroutine(ShowProjectileFireRoutine());
		StopCoroutine(ShowRayRoutine());
		StopCoroutine(ShowHitRoutine());
		gameObject.SetActive(false);
	}

	public override void Update() {
		var axisVertical = GameHandler.AxisVertical;
		var movableBehaviour = Entity.GetComponent<MovableComponent>();
		SetActiveJet(axisVertical > 0);
		UpdateAngle(movableBehaviour.Angle);
	}

	public void TryShowProjectileFire() {
		if ( !gameObject.activeSelf ) {
			return;
		}

		if ( !_projectileFire ) {
			return;
		}
		_projectileVisualFireTimer = _config.ProjectileVisualFireTime;

		if ( !_projectileFire.activeSelf ) {
			StartCoroutine(ShowProjectileFireRoutine());
		}
		_projectileFire.SetActive(true);
	}

	public void ShowRay() {
		if ( !gameObject.activeSelf ) {
			return;
		}
		if ( !_ray ) {
			return;
		}
		_rayVisualFireTimer = _config.RayVisualFireTime;

		if ( !_ray.gameObject.activeSelf ) {
			StartCoroutine(ShowRayRoutine());
		}
		_ray.gameObject.SetActive(true);
	}

	public void ShowHit() {
		if ( !gameObject.activeSelf ) {
			return;
		}
		StopCoroutine(ShowHitRoutine());
		_hitTimer = _config.HitVisualTime;
		_ship.color = Color.red;
		StartCoroutine(ShowHitRoutine());
	}

	void InitModules() {
		if ( _jet ) {
			_jet.gameObject.SetActive(false);
		}
		if ( _projectile ) {
			_projectile.gameObject.SetActive(false);
		}
		if ( _projectileFire ) {
			_projectileFire.gameObject.SetActive(false);
		}
		if ( _ray ) {
			_ray.gameObject.SetActive(false);
		}
		_ship.color = Color.white;
	}

	void SetActiveJet(bool value) {
		if ( _jet ) {
			_jet.SetActive(value);
		}
	}

	void UpdateAngle(float angle) {
		_ship.transform.rotation = Quaternion.Euler(Vector3.forward * (angle - _config.InitAngle));
	}

	IEnumerator ShowProjectileFireRoutine() {
		while ( _projectileVisualFireTimer > 0 ) {
			_projectileVisualFireTimer -= Time.deltaTime;
			yield return null;
		}
		_projectileFire.SetActive(false);
	}

	IEnumerator ShowRayRoutine() {
		while ( _rayVisualFireTimer > 0 ) {
			_rayVisualFireTimer -= Time.deltaTime;
			yield return null;
		}
		_ray.gameObject.SetActive(false);
	}

	IEnumerator ShowHitRoutine() {
		while ( _hitTimer > 0 ) {
			_hitTimer -= Time.deltaTime;
			yield return null;
		}

		_ship.color = Color.white;
	}
}