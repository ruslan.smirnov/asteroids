using UnityEngine;

public abstract class BaseView : MonoBehaviour {
	public BaseEntity Entity { get; private set; }

	public virtual void SetupEntity(BaseEntity entity) {
		Entity = entity;
	}

	public virtual void Update() { }

	public virtual void Hide() { }
}