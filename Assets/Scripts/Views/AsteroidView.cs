using System.Collections;
using UnityEngine;

public enum AsteroidSize {
	Large  = 0,
	Medium = 1,
	Small  = 2,
}

public sealed class AsteroidView : BaseView {
	[SerializeField]
	SpriteRenderer _spriteRenderer;

	[SerializeField]
	AsteroidSize _size;

	float _hitTimer;

	public AsteroidSize Size => _size;

	void OnTriggerEnter2D(Collider2D other) {
		GameHandler.Instance.HandleCollision(this, other);
	}

	public override void Hide() {
		StopCoroutine(ShowHitRoutine());
	}

	public void ShowHit(float time) {
		StopCoroutine(ShowHitRoutine());
		_hitTimer = time;
		_spriteRenderer.color = Color.red;
		StartCoroutine(ShowHitRoutine());
	}

	IEnumerator ShowHitRoutine() {
		while ( _hitTimer > 0 ) {
			_hitTimer -= Time.deltaTime;
			yield return null;
		}

		_spriteRenderer.color = Color.white;
	}
}