using UnityEngine;

public sealed class UfoView : BaseView {
	[SerializeField]
	GameObject _spriteObject;

	float _initAngle;

	void OnTriggerEnter2D(Collider2D other) {
		GameHandler.Instance.HandleCollision(this, other);
	}

	public void Setup() {
		var config = GameHandler.Instance.UnityRepository.UfoConfig;
		_initAngle = config.InitAngle;
	}

	public override void Update() {
		var movableBehaviour = Entity.GetComponent<MovableComponent>();
		UpdateAngle(movableBehaviour.Angle);
	}

	void UpdateAngle(float angle) {
		_spriteObject.transform.rotation = Quaternion.Euler(Vector3.forward * (angle - _initAngle));
	}
}