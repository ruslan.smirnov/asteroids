using UnityEngine;

public sealed class RayView : BaseView {
	void OnTriggerEnter2D(Collider2D other) {
		GameHandler.Instance.HandleCollision(this, other);
	}

	public void Setup(float scale, float position) {
		var currentScale = transform.localScale;
		var currentPosition = transform.localPosition;
		transform.localScale = new Vector3(currentScale.x, scale, currentScale.z);
		transform.localPosition = new Vector3(currentPosition.x, position, currentPosition.z);
	}
}