using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public sealed class EntityHandler {
	const int AsteroidsLargeCount = 3;

	readonly UnityRepository _repository;

	readonly List<EntityContainer> _containers = new List<EntityContainer>();

	readonly MovableBehaviour      _movableBehaviour;
	readonly DestroyableBehaviour  _destroyableBehaviour;
	readonly TeleportableBehaviour _teleportableBehaviour;
	readonly RemovableBehaviour    _removableBehaviour;
	readonly PlayableBehaviour     _playableBehaviour;

	public EntityContainer ShipContainer { get; private set; }
	public PlayableBehaviour PlayableBehaviour => _playableBehaviour;

	public int AsteroidCount { get; private set; }
	public int UfoCount { get; private set; }

	public Action<int> EnemyDestroy;
	public Action      PlayerDestroy;

	public EntityHandler(UnityRepository repository) {
		_repository = repository;

		_movableBehaviour      = new MovableBehaviour();
		_destroyableBehaviour  = new DestroyableBehaviour();
		_teleportableBehaviour = new TeleportableBehaviour();
		_removableBehaviour    = new RemovableBehaviour(this);
		_playableBehaviour     = new PlayableBehaviour();

		var shipContainer = CreateShip();
		var shipView = shipContainer.View as ShipView;

		_playableBehaviour.Setup(_repository.ShipConfig, shipContainer.Entity as ShipEntity, shipView);
	}

	public void Update() {
		var containers = _containers.FindAll(x => x.Entity.GetComponent<MovableComponent>() != null);
		foreach ( var entityContainer in containers ) {
			_movableBehaviour.Update(entityContainer);
		}

		containers = _containers.FindAll(x => x.Entity.GetComponent<TeleportableComponent>() != null);
		foreach ( var entityContainer in containers ) {
			_teleportableBehaviour.Update(entityContainer);
		}

		containers = _containers.FindAll(x => x.Entity.GetComponent<RemovableComponent>() != null);
		foreach ( var entityContainer in containers ) {
			_removableBehaviour.Update(entityContainer);
		}

		var views = _containers.FindAll(x => x.View != null);
		foreach ( var entityContainer in views ) {
			entityContainer.View.Update();
		}

		var asteroids = _containers.FindAll(x => (x.View != null) && (x.View is AsteroidView {Size: AsteroidSize.Large}));
		AsteroidCount = asteroids.Count;

		var ufo = _containers.FindAll(x => (x.View != null) && (x.View is UfoView));
		UfoCount = ufo.Count;
	}

	public void HandleCollision(BaseView view, Collider2D other) {
		var container = GetContrainer(view);

		if ( _destroyableBehaviour.TryHit(container, other.tag) ) {
			if ( view is AsteroidView asteroidView
			     && ((asteroidView.Size == AsteroidSize.Large) || (asteroidView.Size == AsteroidSize.Medium)) ) {
				asteroidView.ShowHit(_repository.ShipConfig.HitVisualTime);
			}

			if ( view is ShipView ) {
				_playableBehaviour.ShowHit();
			}
		}

		if ( container.Entity == null ) {
			return;
		}

		var destroyable = container.Entity.GetComponent<DestroyableComponent>();
		if ( (destroyable != null) && !destroyable.IsAvableDestroy ) {
			return;
		}

		var position = view.transform.position;
		var needSpawnNextMedium = view is AsteroidView {Size: AsteroidSize.Large};
		var needSpawnNextSmall = view is AsteroidView {Size: AsteroidSize.Medium};

		if ( needSpawnNextMedium ) {
			CreateAsteroid(AsteroidSize.Medium, position);
			CreateAsteroid(AsteroidSize.Medium, position);
		}

		if ( needSpawnNextSmall ) {
			CreateAsteroid(AsteroidSize.Small, position);
			CreateAsteroid(AsteroidSize.Small, position);
			CreateAsteroid(AsteroidSize.Small, position);
			CreateAsteroid(AsteroidSize.Small, position);
		}

		// должны перенести в пул inactive
		if ( view is ShipView ) {
			view.Hide();
			PlayerDestroy?.Invoke();
			return;
		}

		if ( !(view is ProjectileView) ) {
			var bounty = container.Entity.GetComponent<DestroyableComponent>().Config.ScoreBounty;
			EnemyDestroy?.Invoke(bounty);
		}

		RemoveContainer(container);
	}

	public void SetupAsterois() {
		for ( var index = 0; index < AsteroidsLargeCount; index++ ) {
			CreateAsteroid(AsteroidSize.Large, _repository.AsteroidLargeView.transform.position);
		}
	}

	public EntityContainer CreateShip() {
		var container = GetContrainer(_repository.ShipView);

		ShipEntity entity = container.Entity as ShipEntity;
		MovableComponent movable;
		DestroyableComponent destroyable;
		TeleportableComponent teleportable;

		if ( entity == null ) {
			entity = new ShipEntity();
			movable = new MovableComponent();
			destroyable = new DestroyableComponent();
			teleportable = new TeleportableComponent();

			entity.AddComponent(movable);
			entity.AddComponent(destroyable);
			entity.AddComponent(teleportable);
			ShipContainer = AddContainer(entity, _repository.ShipView);
		} else {
			movable = entity.GetComponent<MovableComponent>();
			destroyable = entity.GetComponent<DestroyableComponent>();
			teleportable = entity.GetComponent<TeleportableComponent>();
		}

		movable.Setup(_repository.ShipConfig, _repository.ShipView.transform, _repository.ShipConfig.InitAngle);
		destroyable.Setup(_repository.ShipDestroyableConfig);
		teleportable.Setup(_repository.ShipView.transform);

		_repository.ShipView.gameObject.SetActive(true);
		_repository.ShipView.SetupEntity(entity);
		_repository.ShipView.Setup();

		return ShipContainer;
	}

	public EntityContainer CreateProjectile() {
		var view = _repository.CreateView(_repository.ProjectileView);
		var entity = new ProjectileEntity();
		var movable = new ProjectileMovableComponent();
		var destroyable = new DestroyableComponent();
		var removable = new RemovableComponent();

		var angle = ShipContainer.Entity.GetComponent<MovableComponent>().Angle;

		movable.Setup(_repository.ProjectileConfig, view.transform, angle);
		destroyable.Setup(_repository.ProjectileDestroyableConfig);
		removable.Setup(view.transform);

		entity.AddComponent((MovableComponent) movable);
		entity.AddComponent(destroyable);
		entity.AddComponent(removable);

		view.transform.position = _repository.ProjectileView.transform.position;
		view.gameObject.SetActive(true);

		view.SetupEntity(entity);
		return AddContainer(entity, view);
	}

	public EntityContainer CreateAsteroid(AsteroidSize asteroidSize, Vector3 position) {
		AsteroidView view = null;
		MovableConfig movableConfig = null;
		DestroyableConfig destroyableConfig = null;

		switch ( asteroidSize ) {
			case AsteroidSize.Large: {
				view = _repository.CreateView(_repository.AsteroidLargeView);
				movableConfig = _repository.AsteroidLargeConfig;
				destroyableConfig = _repository.AsteroidLargeDestroyableConfig;
				break;
			}
			case AsteroidSize.Medium: {
				var count = _repository.AsteroidMediumView.Count;
				var id = Random.Range(0, count);
				view = _repository.CreateView(_repository.AsteroidMediumView[id]);
				movableConfig = _repository.AsteroidMediumConfig;
				destroyableConfig = _repository.AsteroidMediumDestroyableConfig;
				break;
			}
			case AsteroidSize.Small: {
				var count = _repository.AsteroidMediumView.Count;
				var id = Random.Range(0, count);
				view = _repository.CreateView(_repository.AsteroidSmallView[id]);
				movableConfig = _repository.AsteroidSmallConfig;
				destroyableConfig = _repository.AsteroidSmallDestroyableConfig;
				break;
			}
		}

		var entity = new AsteroidEntity();
		var movable = new AsteroidMovableComponent();
		var destroyable = new DestroyableComponent();
		var teleportable = new TeleportableComponent();

		var angle = Random.Range(0, 360);
		movable.Setup(movableConfig, view.transform, angle);
		destroyable.Setup(destroyableConfig);
		teleportable.Setup(view.gameObject.transform);

		entity.AddComponent((MovableComponent) movable);
		entity.AddComponent(destroyable);
		entity.AddComponent(teleportable);

		view.transform.position = position;
		view.gameObject.SetActive(true);

		view.SetupEntity(entity);
		return AddContainer(entity, view);
	}

	public EntityContainer CreateUfo(Vector3 position) {
		var view = _repository.CreateView(_repository.UfoView);

		var entity = new UfoEntity();
		var movable = new UfoMovableComponent();
		var destroyable = new DestroyableComponent();

		movable.Setup(_repository.UfoConfig, view.gameObject.transform,  _repository.UfoConfig.InitAngle);
		destroyable.Setup(_repository.UfoDestroyableConfig);

		entity.AddComponent((MovableComponent) movable);
		entity.AddComponent(destroyable);

		view.transform.position = position;
		view.gameObject.SetActive(true);
		view.Setup();

		view.SetupEntity(entity);
		return AddContainer(entity, view);
	}

	EntityContainer AddContainer(BaseEntity entity, BaseView view) {
		var container = new EntityContainer(entity, view);
		_containers.Add(container);
		return container;
	}

	public void RemoveContainer(EntityContainer container) {
		// у Entity-View должен быть id для быстрого поиска
		_repository.HideView(container.View);
		container.View.SetupEntity(null);
		_containers.Remove(container);
	}

	public EntityContainer GetContrainer(BaseView view) {
		// у Entity-View должен быть id для быстрого поиска
		return _containers.Find(x => x.View == view);
	}
}