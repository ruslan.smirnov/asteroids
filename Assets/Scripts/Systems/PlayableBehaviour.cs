using System;
using UnityEngine;

public sealed class PlayableBehaviour {
	public ShipConfig Config { get; private set; }
	public ShipView View { get; private set; }
	public int   Chargers { get; private set; }
	public float ReloadTimer { get; private set; }

	public Action<float>    UpdateAngle;
	public Action<Vector2>  UpdatePosition;
	public Action<int, int> UpdateChargers;
	public Action<float>    UpdateReloadTimer;
	public Action<float>    UpdateSpeed;
	public Action<int>      UpdateHealth;

	float                _projectileReloadTimer;
	MovableComponent     _movable;
	DestroyableComponent _destroyable;

	public void Setup(ShipConfig config, ShipEntity entity, ShipView view) {
		Config = config;
		View = view;
		Chargers = Config.RayCharges;

		_movable = entity.GetComponent<MovableComponent>();
		_destroyable = entity.GetComponent<DestroyableComponent>();
	}

	public void Update() {
		if ( _projectileReloadTimer > 0 ) {
			_projectileReloadTimer -= Time.deltaTime;
		}
		if ( ReloadTimer > 0 ) {
			UpdateReloadTimer?.Invoke(ReloadTimer);
			ReloadTimer -= Time.deltaTime;
		}

		if ( (ReloadTimer <= 0) && (Chargers < Config.RayCharges) ) {
			Chargers++;
			UpdateChargers?.Invoke(Chargers, Config.RayCharges);
			UpdateReloadTimer?.Invoke(ReloadTimer);
			if ( Chargers < Config.RayCharges ) {
				ReloadTimer = Config.RayReloadTime;
			}
		}

		if ( _movable.Speed > 0 ) {
			UpdatePosition?.Invoke(_movable.Position);
			UpdateSpeed?.Invoke(_movable.Speed);
		}

		if ( (GameHandler.AxisHorizontal != 0) || (GameHandler.AxisVertical != 0) ) {
			UpdateAngle?.Invoke(_movable.Angle);
		}
	}

	public bool TryShootProjectile() {
		if ( _projectileReloadTimer > 0 ) {
			return false;
		}

		_projectileReloadTimer = Config.ProjectileReloadTime;

		View.TryShowProjectileFire();

		return true;
	}

	public bool TryShootRay() {
		if ( Chargers <= 0 ) {
			return false;
		}

		if ( ReloadTimer <= 0 ) {
			ReloadTimer = Config.RayReloadTime;
		}
		Chargers--;
		UpdateChargers?.Invoke(Chargers, Config.RayCharges);

		View.ShowRay();

		return true;
	}

	public void ShowHit() {
		var healthPoints = _destroyable.HealthPoints;
		if ( healthPoints > 0 ) {
			View.ShowHit();
		}
		UpdateHealth?.Invoke(healthPoints);
	}
}