using UnityEngine;

public sealed class MovableBehaviour : BaseBehaviour {
	public override void Update(EntityContainer container) {
		var component = container.Entity.GetComponent<MovableComponent>();
		component?.Update();
	}
}