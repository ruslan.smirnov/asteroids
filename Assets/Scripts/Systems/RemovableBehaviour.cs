using UnityEngine;

public sealed class RemovableBehaviour : BaseBehaviour {
	readonly EntityHandler   _entityHandler;

	public RemovableBehaviour(EntityHandler entityHandler) {
		_entityHandler = entityHandler;
	}

	public override void Update(EntityContainer container) {
		var component = container.Entity.GetComponent<RemovableComponent>();
		if ( component == null ) {
			return;
		}

		var transform = component.Transform;
		var position = transform.position;

		Vector3 viewPos = Camera.main.WorldToViewportPoint(position);
		var horizontal = (viewPos.x >= 0) && (viewPos.x <= 1);
		var vertical = (viewPos.y >= 0) && (viewPos.y <= 1);

		if ( !horizontal || !vertical ) {
			_entityHandler.RemoveContainer(container);
		}
	}
}