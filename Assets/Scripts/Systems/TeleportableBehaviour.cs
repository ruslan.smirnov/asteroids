using UnityEngine;

public sealed class TeleportableBehaviour : BaseBehaviour {

	public override void Update(EntityContainer container) {
		var gh = GameHandler.Instance;
		var component = container.Entity.GetComponent<TeleportableComponent>();
		if ( component == null ) {
			return;
		}

		var transform = component.Transform;

		var widthHalf = gh.GameField.WidthHalf;
		var heightHalf = gh.GameField.HeightHalf;

		var limit = GameField.TeleportAngularLimit;
		var position = transform.position;

		var leftTop = new Vector2(-1 * widthHalf, heightHalf);
		var leftBottom = new Vector2(-1 * widthHalf, -1 * heightHalf);
		var rightTop = new Vector2(widthHalf, heightHalf);
		var rightBottom = new Vector2(widthHalf, -1 * heightHalf);

		var leftTopOffset = new Vector2(widthHalf * -1 + limit, heightHalf - limit);
		var leftBottomOffset = new Vector2(widthHalf * -1 + limit, heightHalf * -1 + limit);
		var rightTopOffset = new Vector2(widthHalf - limit, heightHalf - limit);
		var rightBottomOffset = new Vector2(widthHalf - limit, heightHalf * -1 + limit);

		if ( InCircle(position, leftTop, limit) ) {
			var correct = new Vector3(rightBottomOffset.x, rightBottomOffset.y, position.z);
			transform.position = correct;
			return;
		}

		if ( InCircle(position, leftBottom, limit) ) {
			var correct = new Vector3(rightTopOffset.x, rightTopOffset.y, position.z);
			transform.position = correct;
			return;
		}

		if ( InCircle(position, rightTop, limit) ) {
			var correct = new Vector3(leftBottomOffset.x, leftBottomOffset.y, position.z);
			transform.position = correct;
			return;
		}

		if ( InCircle(position, rightBottom, limit) ) {
			var correct = new Vector3(leftTopOffset.x, leftTopOffset.y, position.z);
			transform.position = correct;
			return;
		}

		limit = GameField.TeleportlinearLimit;
		Vector3 viewPos = Camera.main.WorldToViewportPoint(position);
		var horizontal = (viewPos.x >= 0) && (viewPos.x <= 1);
		var vertical = (viewPos.y >= 0) && (viewPos.y <= 1);

		if ( !horizontal ) {
			var direction = position.x < 0 ? 1 : -1;
			var offset = direction * limit * -1;
			var correct = new Vector3(widthHalf * direction + offset, position.y, position.z);
			transform.transform.position = correct;
			return;
		}
		if ( !vertical ) {
			var direction = position.y < 0 ? 1 : -1;
			var offset = direction * limit * -1;
			var correct = new Vector3(position.x, heightHalf * direction + offset, position.z);
			transform.transform.position = correct;
			return;
		}
	}

	bool InCircle(Vector2 point, Vector2 circlePoint, float radius) {
		return (point - circlePoint).sqrMagnitude <= radius * radius;
	}
}