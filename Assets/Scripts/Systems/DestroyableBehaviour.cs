public sealed class DestroyableBehaviour : BaseBehaviour {

	public bool TryHit(EntityContainer container, string tag) {
		var destroyable = container.Entity?.GetComponent<DestroyableComponent>();
		if ( destroyable == null ) {
			return false;
		}
		return destroyable.TryDecHealth(tag);
	}
}